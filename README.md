### Hello there

Hi! i'm Sendokame, a 14 years old "programmer", i started at the age of 12 with **Batch**.<br>

# What i know?
#### Languages
- Python
- JavaScript
- PowerShell

#### Technologies
- HTML
- CSS
- Flask
- Heroku

# What i'm learning?
- Git
- Ruby

# More interesting Projects
- [ViCl](https://github.com/ZSendokame/ViCl): Simple API to manage files remotely.
- [Arguing](https://github.com/zsendokame/Arguing): Argument parsing made simple, no bloat.
- [My webpage](https://sendokame.netlify.app): My own webpage on spanish, I talk about hacking and programming.

# Social Media
<a href="https://discord.gg/aBsCR6pyZj"><img src="https://img.shields.io/badge/Discord-7289DA?style=for-the-badge&logo=discord&logoColor=white"/></a>

# My statistics!
[![Sendokame's GitHub stats](https://github-readme-stats.vercel.app/api?username=zsendokame)](https://github.com/zsendokame/zsendokame)
